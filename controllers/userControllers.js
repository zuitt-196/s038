const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.registerUser = (req,res)=>{


	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);



	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


module.exports.getUserDetails = (req,res)=>{
	console.log(req.user)
	//find() will return an array of documents which matches the criteria.
	//It will return an array even if it only found 1 document.
	/*User.find({_id:req.body.id})*/

	//findOne() will return a single document that matched our criteria.
	//User.findOne({_id:req.body.id})

	//findById() is a mongoose method that will allow us to find a document strictly by its id.
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res) => {

	console.log(req.body);

	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if it is not able to match any document
		if(foundUser === null){
			return res.send({message: "No User Found."})
			//Client will receive this object with our message if no user is found
		} else {
			//console.log(foundUser)
			//If we find a user, foundUser will contain the document that matched the input email.
			//Check if the input password from req.body matches the hashed password in our foundUser document.
			/*
				bcrypt.compareSync(<inputString>,<hashedString>)

				"spidermanOG"
				"$2b$10$ejEI0sqoMtvfdPpTfpzjjewB7525F0swxpXvatMD6nvS60WVTmVbm"

				If the inputString and the hashedString matches and are the same, the compareSync method will return true.
				else, it will return false.
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			//console.log(isPasswordCorrect);

			//IF the password if correct, we will create a "key", a token for our user, else, we will send a message: 
			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.js.

					This module will create a encoded string which contains our users's details.

					This encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly unwrapped or decoded with our own secret word/string.

				*/

				// console.log("We will create a token for the user if the password is correct")

				//auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client.
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {

				return res.send({message: "Incorrect Password"});
			}

		}

	})


}
// Activity

module.exports.getUserByEmail = (req,res)=>{
	// console.log(req.body)

	User.findOne({email:req.body.email})
	.then(result => {
		if (result === null){
			return res.send(false)		
		}else {
			return res.send(true)
		}	
	})
	.catch(error => res.send(error))

}

module.exports.enroll = async (req,res) => {

console.log(req.user.id);

console.log(req.body.courseId);

// Validate the user is an admin or not.

if (req.user.isAdmin){
	return res.send({message: "Action Forbidden."});
	}

	// async and await - async keyword is added to a function to make our function asynchronous. Which means that instead of JS regular behaviour of running 

	// return a boolean to our isUserUpdated variable to determine the result of the query an if 

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// console.log(user);

		let newEnrollment = {
			courseId: req.body.courseId

		}

		user.enrollments.push(newEnrollment)

		return user.save().then (user => true) .catch(err => err.message)

	})
	console.log(isUserUpdated);
}






































// /*
// 	Naming convention for controllers is that it should be named after the model/documents it is concerned with

// 	Controllers are functions which contain the actual business logic of our API and is triggered by a route

// 	MVC - models views and controller
// */

// //To create a controller, we first add it into our module.exports
// // So that we can import the controllers from our module

// // import the user model in the controllers instead because this where are now going to use it
// const User = require("../models/User");
// //Import bcrypt
// //bcrypt is a package which allows us to hash our passwords to add a layer of security for our's 
// const bcrypt = require("bcrypt");

// // import auth.js module to create createAccesToken  and its subsequent metthods

// const auth = require("../auth");

// module.exports.registerUser = (req,res)=> {

// 	//console.log(req.body) //check the input passed via the client
// 	// using bcrypt , hide the users password underneath the a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

// 	//bcrypt 
// 	const hashedPw = bcrypt.hashSync(req.body.password,10);
// 	console.log(hashedPw);

// 	let newUser = new User ({
// 		firstName: req.body.firstName,
// 		lastName: req.body.lastName,
// 		email:req.body.email,
// 		password:hashedPw,
// 		mobileNo: req.body.mobileNo
// 	})

// 	newUser.save()
// 	.then(result => res.send(result))
// 	.catch(error => res.send(error))
// }

// // getUserDetails should only allow the LOGGED-in user to grt his OWN details
// module.exports.getUserDetails = (req,res)=>{
// 	// console.log(req.user)// contains the deatails of the looged in user
// 	//find() will return an array of documents which matches the criteria
// 	// It will return an array even if it only found 1 document.
// 	//User.findOne({_id:req.body.id})

// 	//findOne() will return a single document that matchd our criteria
// 	//User.findOne({_id:req.body.id})

// 	//findById() is a mongoose method that will allow us to find a document strictly by its id
// 	User.findById(req.body.id)
// 	.then(result => res.send(result))
// 	.catch(error => res.send(error))

// }

// module.exports.loginUser = (req,res)=>{
// 	console.log(req.body);


// /*
// 	steps for login in our user session
// 	1. if we found the user, we will  check his password  if the passwrod
// */


// // 
// User.findOne({email:req.body.email})
// .then(foundUser =>{
		

// 	if(foundUser === null){
// 		return res.send({message: "No user Found!"});
// 	}else{
// 		// console.log(foundUser); if we fin
// 	}
	
// 	const isPasswordCorrect = bcrypt.compareSync(req.body.password,
// 		foundUser.password)

// 	// console.log(isPasswordCorrect)
	
// 	if(isPasswordCorrect){
// 			/**
// 			 * to be able create a "key " or token that allows/authorize
// 			 * 
// 			 */	
// 				// 
// 				return res.send({accessToken: auth.createAccesToken(foundUser)});

// 			// console.log("we will create a token for the user if the password is correct")
// 	}else{
// 			return res.send({message:"Incorrect password"})
// 	}
// })

// }
// // ckeckh if email exist or not 
// module.exports.getcheckEmail = (req,res) => {
	
// 	User.find({email:req.body.email})
// 	.then(result =>{

// 		// findONe will return null if no match is found
// 		// send false if email does not exits
// 		// send true if email exits
// 		if(result === null){
// 			return res.send(false)
// 		}else{
// 			return res.send(true)
// 		}
// 	})
// 	.catch(error => res.send(error))

// }
