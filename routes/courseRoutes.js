

/*
	To be able to create routes from another file to be used



	the Router() method will us to container our routes
*/


const express = require("express");
const router = express.Router();

//All route to courses now has an endpoint prefaced with /courses 
//endpoint - /courses/courses

const courseControllers = require("../controllers/courseControllers");



// import the  route 
const auth = require("../auth")
const {verify,verifyAdmin} = auth;


router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);
//endpoint /courses/
//only logged is user is able to use addCourse
//
	router.post('/',verify,verifyAdmin,courseControllers.addCourse);

    // get all active course - (regular, non -loggeg in)
    router.get('/activeCourses', courseControllers.getActiveCourses)

	// get single course
	//;:id route params
	router.get("/getSingleCourse/:courseId", courseControllers.getSingleCourse);



	// update a single course 
	// pass the id of the course we wasnr to updtae via route paraams
	// the update details will be passsed via request body.
	router.put("/updateCourse/:courseId",verify,verifyAdmin,courseControllers.updateCourse)


	// archive a single course to the
	// pass the id for the course we want to, update via routes params 
	// we will direclty updatye the course as inactive 

	router.delete("/archiveCourse/:courseId",verify,verifyAdmin, courseControllers.archiveCourse)

module.exports = router;